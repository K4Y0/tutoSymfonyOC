<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace OC\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/**
 * Description of AdvertController
 *
 * @author Kay
 */
class AdvertController extends Controller{
    
    public function indexAction($page){
        /*
         * $this->get('nom-du-service') return un objet dont le nom est nom-de-servie ici(templating)
         * L'objet templating permet de récupérer le contenu d'un template grace à render 
         */
        /*//On veut avoir l'url de l'annonce d'id 5
        $url = $this->get('router')->generate(
                'oc_platform_view', // nom de la route
                array("id"=>5), //params,
                UrlGeneratorInterface::ABSOLUTE_URL
        );
        $content = $this->get('templating')->render('OCPlatformBundle:Advert:index.html.twig',
                array("nom"=>"Aymeric"));*/
        if($page < 1){
            throw new NotFoundHttpException('Page "'.$page.'"inexistante.');
        }
        
          $listAdverts = array(
      array(
        'title'   => 'Recherche développpeur Symfony',
        'id'      => 1,
        'author'  => 'Alexandre',
        'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
        'date'    => new \Datetime()),
      array(
        'title'   => 'Mission de webmaster',
        'id'      => 2,
        'author'  => 'Hugo',
        'content' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
        'date'    => new \Datetime()),
      array(
        'title'   => 'Offre de stage webdesigner',
        'id'      => 3,
        'author'  => 'Mathieu',
        'content' => 'Nous proposons un poste pour webdesigner. Blabla…',
        'date'    => new \Datetime())
    );
        
        return $this->render('OCPlatformBundle:Advert:index.html.twig', array('listAdverts'=> $listAdverts));
    }
    
    public function viewAction($id, Request $request){
        $advert = array(
            'title'   => 'Recherche développpeur Symfony2',
            'id'      => $id,
            'author'  => 'Alexandre',
            'content' => 'Nous recherchons un développeur Symfony2 débutant sur Lyon. Blabla…',
            'date'    => new \Datetime()
          );

          return $this->render('OCPlatformBundle:Advert:view.html.twig', array(
            'advert' => $advert
          ));
    }
    
    public function addAction(Request $request){
        
        if($request->isMethod('POST')){
            $request->getSession()->getFlashBag()->add('notice', 'Annoce enregistré');
            return $this->redirectToRoute('oc_platform_view', array('id'=>5));

        }
        
        return $this->render('OCPlatformBundle:Advert:add.html.twig');
    }
    
    public function editAction($id, Request $request){
        if($request->isMethod('POST')){
            $request->getSession()->getFlashBag()->add('notice', 'Annonce modifié');
            return $this->redirectToRoute('oc_platform_view', array($id=>5));
        }
        
        $advert = array(
            'title'   => 'Recherche développpeur Symfony',
            'id'      => $id,
            'author'  => 'Alexandre',
            'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
            'date'    => new \Datetime()
          );
        
        return $this->render("OCPlatformBundle:Advert:edit.html.twig", array('advert'=>$advert));
    }
    
    public function deleteAction($id){
        return $this->render("OCPlatformBundle:Advert:delete.html.twig");
    }
    
    public function menuAction(){
        $listAdverts = array(
            array('id'=>2, 'title'=> 'Recherche développeur Symfony'),
            array('id'=>5, 'title'=> 'Mission webmaster'),
            array('id'=>9, 'title'=> 'Offre de stage webdesigner')
        );
        
        return $this->render('OCPlatformBundle:Advert:menu.html.twig', array(
            'listAdverts'=>$listAdverts
        ));
    }
    
    public function viewOldAction($id, Request $request){
        
        $tag = $request->query->get('tag');
        
        if($request->isMethod('Post')){
            return new Response("is POST");
        }
        
        /*$response = new Response();
        $response->setContent("Ceci est une page 404");
        $response->setStatusCode(Response::HTTP_NOT_FOUND);*/
        $content = $this->get('templating')->render('OCPlatformBundle:Advert:view.html.twig',
                array('id'=>$id, 'tag'=>$tag));
        return new JsonResponse(array("id"=>$id));
        //$url = $this->get('router')->generate('oc_platform_home');
        //return new RedirectResponse($url); // retourne une redirection 
        
        // ou
       // return $this->redirectToRoute('oc_platform_home');
        
    }
    
}
